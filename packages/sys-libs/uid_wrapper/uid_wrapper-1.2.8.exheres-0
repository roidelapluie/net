# Copyright 2015-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake

SUMMARY="A wrapper for privilege separation"
DESCRIPTION="
* Allows uid switching as a normal user.
* Start any application making it believe it is running as root.
* Support for user/group changing in the local thread using the syscalls (like glibc).
* More precisely this library intercepts seteuid and related calls, and simulates them in a manner
  similar to the nss_wrapper and socket_wrapper libraries.

Some projects like a file server need privilege separation to be able to switch to the connection
user and do file operations. uid_wrapper convincingly lies to the application letting it believe it
is operating as root and even switching between UIDs and GIDs as needed.
"
HOMEPAGE="https://cwrap.org/${PN}.html"
DOWNLOADS="mirror://samba/../cwrap/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-util/cmocka[>=1.1.0]
"

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DUNIT_TESTING:BOOL=TRUE DUNIT_TESTING:BOOL=FALSE'
)

