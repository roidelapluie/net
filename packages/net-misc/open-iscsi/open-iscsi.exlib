# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=${PN} tag=${PV} ]

export_exlib_phases src_prepare src_install

SUMMARY="An IP-based storage networking standard for linking data storage facilities"
DESCRIPTION="
The Open-iSCSI project is a high performance, transport independent, multi-platform
implementation of RFC3720, the RFC that specifies the Internet Small Computer
Systems Interface (iSCSI).
Linux-iSCSI was merged with Open-iSCSI in 2005 and, thus, is obsolete.
"
HOMEPAGE="https://github.com/open-iscsi/open-iscsi"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""
DEPENDENCIES="
    build+run:
        net-misc/open-isns
"

DEFAULT_SRC_COMPILE_PARAMS=( user )

open-iscsi_src_prepare() {
    default

    edo sed -i -e "s#pkg-config#${PKG_CONFIG}#" iscsiuio/configure.ac
}

open-iscsi_src_install() {
    edo mkdir "${IMAGE}"/etc{,/iscsi}
    emake -j1 DESTDIR="${IMAGE}" install \
        prefix=/usr/$(exhost --target) \
        exec_prefix=/usr/$(exhost --target) \
        sbindir=/usr/$(exhost --target)/bin \
        mandir=/usr/share/man \
        LIB_DIR=/usr/$(exhost --target)/lib
    emagicdocs
}

