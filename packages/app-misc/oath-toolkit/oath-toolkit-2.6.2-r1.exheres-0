# Copyright 2014 Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu>
# Distributed under the terms of the GNU General Public License v2

require pam autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.13 ] ]

SUMMARY="One-time password components"
DESCRIPTION="
The OATH Toolkit provide components for building one-time password authentication systems, amongst
which shared libraries, command line tools and a PAM module. It supports the event-based HOTP
algorithm (RFC4226) and the time-based TOTP algorithm (RFC6238).
"
HOMEPAGE="https://www.nongnu.org/${PN}/"
DOWNLOADS="https://download.savannah.gnu.org/releases/${PN}/${PNV}.tar.gz"

LICENCES="LGPL-2 GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
      dev-libs/libxml2:2.0
      dev-libs/xmlsec
      sys-libs/pam
"

# Needed for glibc[>=2.28]
DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/update-gnulib-files.patch
    "${FILES}"/fseeko-glibc-2-28.patch
)
# Shouldn't be needed after the patches above get merged
AT_M4DIR=( gl/m4 )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-pam
    --enable-pskc
    --disable-static
    --disable-xmltest
    --with-pam-dir=$(getpam_mod_dir)
)

