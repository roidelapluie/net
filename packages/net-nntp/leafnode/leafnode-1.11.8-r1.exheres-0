# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Based in part upon 'leafnode-1.11.7.ebuild' from Gentoo, which is:
#   Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix="tar.xz" ]

SUMMARY="NNTP server designed for small sites"
DESCRIPTION="
Leafnode is a software package that implements a store & forward NNTP proxy
(client and server) that supports TCP connections across IPv4 and IPv6. It can
be used to give a regular newsreader off-line functionality, merge news
articles from several upstream newsservers for newsreaders that only support
one server well and avoid duplicate news download for a small LAN with multiple
users reading news.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/pcre
    build+run:
        group/news
        user/news
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --sysconfdir=/etc/${PN}
    --with-ipv6
    --with-spooldir=/var/spool/news
    --with-user=news
    --with-group=news
)

src_install() {
    default

    keepdir \
        /var/lib/news \
        /var/spool/news/{failed.postings,interesting.groups,leaf.node,out.going,temp.files} \
        /var/spool/news/message.id/{0..9}{0..9}{0..9}

    edo chown -R news:news "${IMAGE}"/var/{lib,spool}/news

    exeinto /etc/cron.daily
    doexe "${FILES}"/{fetchnews,texpire}.cron
}
