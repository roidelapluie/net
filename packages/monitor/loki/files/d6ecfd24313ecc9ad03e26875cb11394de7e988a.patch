Source/Upstream: Yes, fixed in git master
Reason: Fix problem using go 1.16

From d6ecfd24313ecc9ad03e26875cb11394de7e988a Mon Sep 17 00:00:00 2001
From: Cyril Tovena <cyril.tovena@gmail.com>
Date: Tue, 16 Mar 2021 03:58:17 -0400
Subject: [PATCH] Set the byte slice cap correctly when unsafely converting
 string. (#3487)

Fixes #3485

Should also unblock #3481

When unsafely converting string to bytes the cap of the bytes is lost but not when doing bytes to string.
It seems that this is only a problem when using go 1.16 or when using delve.

Signed-off-by: Cyril Tovena <cyril.tovena@gmail.com>
---
 pkg/logql/log/pipeline.go      | 13 ++++---
 pkg/logql/log/pipeline_test.go | 65 +++++++++++++++++++++++++++++-----
 2 files changed, 64 insertions(+), 14 deletions(-)

diff --git a/pkg/logql/log/pipeline.go b/pkg/logql/log/pipeline.go
index d7ae619a96..dbeaad79a8 100644
--- a/pkg/logql/log/pipeline.go
+++ b/pkg/logql/log/pipeline.go
@@ -1,15 +1,14 @@
 package log
 
 import (
+	"reflect"
 	"unsafe"
 
 	"github.com/prometheus/prometheus/pkg/labels"
 )
 
-var (
-	// NoopStage is a stage that doesn't process a log line.
-	NoopStage Stage = &noopStage{}
-)
+// NoopStage is a stage that doesn't process a log line.
+var NoopStage Stage = &noopStage{}
 
 // Pipeline can create pipelines for each log stream.
 type Pipeline interface {
@@ -179,7 +178,11 @@ func ReduceStages(stages []Stage) Stage {
 }
 
 func unsafeGetBytes(s string) []byte {
-	return *(*[]byte)(unsafe.Pointer(&s))
+	var buf []byte
+	p := unsafe.Pointer(&buf)
+	*(*string)(p) = s
+	(*reflect.SliceHeader)(p).Cap = len(s)
+	return buf
 }
 
 func unsafeGetString(buf []byte) string {
diff --git a/pkg/logql/log/pipeline_test.go b/pkg/logql/log/pipeline_test.go
index 21c80757df..0a923e318c 100644
--- a/pkg/logql/log/pipeline_test.go
+++ b/pkg/logql/log/pipeline_test.go
@@ -49,14 +49,17 @@ func TestPipeline(t *testing.T) {
 }
 
 var (
-	resOK   bool
-	resLine []byte
-	resLbs  LabelsResult
+	resOK         bool
+	resLine       []byte
+	resLineString string
+	resLbs        LabelsResult
+	resSample     float64
 )
 
 func Benchmark_Pipeline(b *testing.B) {
 	b.ReportAllocs()
-	p := NewPipeline([]Stage{
+
+	stages := []Stage{
 		mustFilter(NewFilter("metrics.go", labels.MatchEqual)).ToStage(),
 		NewLogfmtParser(),
 		NewAndLabelFilter(
@@ -67,8 +70,10 @@ func Benchmark_Pipeline(b *testing.B) {
 		NewJSONParser(),
 		NewStringLabelFilter(labels.MustNewMatcher(labels.MatchEqual, ErrorLabel, errJSON)),
 		newMustLineFormatter("Q=>{{.query}},D=>{{.duration}}"),
-	})
+	}
+	p := NewPipeline(stages)
 	line := []byte(`level=info ts=2020-10-18T18:04:22.147378997Z caller=metrics.go:81 org_id=29 traceID=29a0f088b047eb8c latency=fast query="{stream=\"stdout\",pod=\"loki-canary-xmjzp\"}" query_type=limited range_type=range length=20s step=1s duration=58.126671ms status=200 throughput_mb=2.496547 total_bytes_mb=0.145116`)
+	lineString := string(line)
 	lbs := labels.Labels{
 		{Name: "cluster", Value: "ops-tool1"},
 		{Name: "name", Value: "querier"},
@@ -79,12 +84,54 @@ func Benchmark_Pipeline(b *testing.B) {
 		{Name: "job", Value: "loki-dev/querier"},
 		{Name: "pod_template_hash", Value: "5896759c79"},
 	}
-	b.ResetTimer()
+
 	sp := p.ForStream(lbs)
-	for n := 0; n < b.N; n++ {
-		resLine, resLbs, resOK = sp.Process(line)
-	}
 
+	b.Run("pipeline bytes", func(b *testing.B) {
+		b.ResetTimer()
+		for n := 0; n < b.N; n++ {
+			resLine, resLbs, resOK = sp.Process(line)
+		}
+	})
+	b.Run("pipeline string", func(b *testing.B) {
+		b.ResetTimer()
+		for n := 0; n < b.N; n++ {
+			resLineString, resLbs, resOK = sp.ProcessString(lineString)
+		}
+	})
+
+	extractor, err := NewLineSampleExtractor(CountExtractor, stages, []string{"cluster", "level"}, false, false)
+	require.NoError(b, err)
+	ex := extractor.ForStream(lbs)
+	b.Run("line extractor bytes", func(b *testing.B) {
+		b.ResetTimer()
+		for n := 0; n < b.N; n++ {
+			resSample, resLbs, resOK = ex.Process(line)
+		}
+	})
+	b.Run("line extractor string", func(b *testing.B) {
+		b.ResetTimer()
+		for n := 0; n < b.N; n++ {
+			resSample, resLbs, resOK = ex.ProcessString(lineString)
+		}
+	})
+
+	extractor, err = LabelExtractorWithStages("duration", "duration", []string{"cluster", "level"}, false, false, stages, NoopStage)
+	require.NoError(b, err)
+	ex = extractor.ForStream(lbs)
+
+	b.Run("label extractor bytes", func(b *testing.B) {
+		b.ResetTimer()
+		for n := 0; n < b.N; n++ {
+			resSample, resLbs, resOK = ex.Process(line)
+		}
+	})
+	b.Run("label extractor string", func(b *testing.B) {
+		b.ResetTimer()
+		for n := 0; n < b.N; n++ {
+			resSample, resLbs, resOK = ex.ProcessString(lineString)
+		}
+	})
 }
 
 func mustFilter(f Filterer, err error) Filterer {
