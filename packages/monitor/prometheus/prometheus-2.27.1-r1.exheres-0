# Copyright 2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] \
    flag-o-matic \
    systemd-service

SUMMARY="Monitoring system and time series database"
DOWNLOADS+=" https://dev.gentoo.org/~zlogene/distfiles/app-metrics/${PN}/${PNV}-asset.tar.xz"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# TODO: need to figure out
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.14]
    build+run:
        group/prometheus
        user/prometheus
"

pkg_setup() {
    filter-ldflags -Wl,-O1 -Wl,--as-needed
}

src_unpack() {
    default

    edo pushd "${WORK}"
    esandbox disable_net
    edo go mod download -x
    esandbox enable_net
    edo popd
}

src_prepare() {
    default

    export GOROOT="/usr/$(exhost --target)/lib/go"
    export GOPATH="${WORKBASE}"/build

    edo mkdir -p "${WORKBASE}"/build/src/github.com/prometheus
    edo ln -s "${WORK}" "${WORKBASE}"/build/src/github.com/prometheus/prometheus

    edo ln -s "${TEMP}"/go/pkg "${WORKBASE}"/build

    # pre-built assets
    edo mv ../assets_vfsdata.go web/ui
}

src_compile() {
    GO111MODULE="auto" edo go build \
        -o bin/prometheus \
        -tags netgo,builtinassets \
        -ldflags "${LDFLAGS} -X github.com/prometheus/common/version.Version=${PV}" \
        -v \
        "./cmd/prometheus"

    GO111MODULE="auto" edo go build \
        -o bin/promtool \
        -ldflags "${LDFLAGS} -X github.com/prometheus/common/version.Version=${PV}" \
        -v \
        "./cmd/promtool"
}

src_install() {
    dobin bin/{prometheus,promtool}

    insinto /etc/default
    doins "${FILES}"/${PN}

    insinto /etc/${PN}
    doins documentation/examples/${PN}.yml
    doins -r console_libraries consoles

    install_systemd_files

    keepdir /var/{lib,log}/${PN}
    edo chown -R prometheus:prometheus "${IMAGE}"/var/{lib,log}/${PN}
}

