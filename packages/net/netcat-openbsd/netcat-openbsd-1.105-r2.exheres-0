# Copyright 2015 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

SUMMARY="OpenBSD netcat"
DESCRIPTION="
OpenBSD netcat that supports both IPv4 and IPv6, UNIX-domain sockets,
TCP and UDP
"

DEBIAN_REV="7"
#we use the debian packaged source, since that is verified to work on Linux
HOMEPAGE="http://www.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man1/nc.1?query=nc"
DOWNLOADS="
    http://ftp.debian.org/debian/pool/main/n/${PN}/${PN}_${PV}.orig.tar.gz
    http://ftp.debian.org/debian/pool/main/n/${PN}/${PN}_${PV}-${DEBIAN_REV}.debian.tar.gz
"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/libbsd
    run:
        (
            !net-misc/netcat[<0.7.1-r1]
            !net-misc/netcat6[<1.0-r3]
        ) [[
            *description = [ Both install /usr/bin/nc and man pages ]
            resolution = upgrade-blocked-before
        ]]
"

src_prepare(){
    expatch ../debian/patches/
    edo sed -i -e '/pkg-config/i \
PKG_CONFIG ?= pkg-config' Makefile
    edo sed -i -e 's/pkg-config/\${PKG_CONFIG\}/' Makefile
}

src_compile(){
    emake \
    CFLAGS="$CFLAGS -DDEBIAN_VERSION=\"\\\"${DEBIAN_REV}\\\"\" -I/usr/include/libbsd" \
    LDFLAGS="$LDFLAGS -lbsd"
}

src_install(){
    edo rm Makefile
    default
    dobin nc
    doman nc.1
    dodoc ../debian/copyright
    dodoc ../debian/netcat-openbsd.README.Debian
    dodoc ../debian/changelog

    alternatives_for netcat openbsd 500 \
        /usr/$(exhost --target)/bin/nc netcat-openbsd \
        /usr/share/man/man1/nc.1 netcat-openbsd.1
}
