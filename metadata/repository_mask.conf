(
    dev-libs/tevent[~scm]
    media-video/rtmpdump[~scm]
    net-analyzer/fail2ban[~scm]
    net-fs/samba[~scm]
    net-im/bitlbee[~scm]
    net-im/bitlbee-steam[~scm]
    net-im/minbif[~scm]
    net-irc/weechat[~scm]
    net-irc/znc[~scm]
    net-libs/jreen[~scm]
    net-libs/libndp[~scm]
    net-libs/miniupnpc[~scm]
    net-libs/nghttp3[~scm]
    net-libs/ngtcp2[~scm]
    net-misc/connman[~scm]
    net-misc/mosh[~scm]
    net-p2p/transmission[~scm]
    net-www/uzbl[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

net-fs/cifs-utils[<=5.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Apr 2012 ]
    token = security
    description = [ CVE-2012-1586 ]
]]

net-misc/tor[<0.2.2.39] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 20 Sep 2012 ]
    token = security
    description = [ Several security problems ]
]]

web-apps/cgit[<0.9.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 May 2013 ]
    token = security
    description = [ CVE-2013-2117 ]
]]

net-libs/libssh[<0.9.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 11 Sep 2020 ]
    token = security
    description = [ CVE-2020-16135 ]
]]

www-servers/nginx[<1.20.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 May 2021 ]
    token = security
    description = [ CVE-2021-23017 ]
]]

net-fs/openafs[~scm] [[
    author = [ Dirk Heinrichs <dirk.heinrichs@altum.de> ]
    date = [ 11 May 2013 ]
    token = scm
    description = [ Masked SCM version ]
]]

net-libs/libmicrohttpd[<0.9.32] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Dec 2013 ]
    token = security
    description = [ CVE-2013-703{8,9} ]
]]

net-proxy/torsocks[~scm] [[
    author = [ Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu> ]
    date = [ 28 Nov 2013 ]
    token = scm
    description = [ This is a scm version for the rewrite of torsocks ]
]]

net-proxy/torsocks[<2] [[
    author = [ Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu> ]
    date = [ 19 Jan 2013 ]
    token = security
    description = [ See https://lists.torproject.org/pipermail/tor-dev/2013-June/004959.html ]
]]

net-remote/FreeRDP[<2.2.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 Jul 2020 ]
    token = security
    description = [ CVE-2020-15103 ]
]]

app-crypt/krb5[<1.18.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Nov 2020 ]
    token = security
    description = [ CVE-2020-28196 ]
]]

net-proxy/squid[<4.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Jul 2019 ]
    token = security
    description = [ CVE-2019-12525, CVE-2019-12527, CVE-2019-12529, CVE-2019-13345 ]
]]

net-misc/openvpn[<2.4.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Jun 2017 ]
    token = security
    description = [ CVE-2017-7508, CVE-2017-7520 CVE-2017-7521, CVE-2017-7522 ]
]]

net-analyzer/tcpdump[<4.99.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Jan 2021 ]
    token = security
    description = [ CVE-2020-8037 ]
]]

net-analyzer/wireshark[<3.4.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Jul 2021 ]
    token = security
    description = [ CVE-2021-22222, wnpa-sec-2021-05 ]
]]

net/net-snmp[<5.7.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Jul 2015 ]
    token = security
    description = [ CVE-2014-3565 ]
]]

web-apps/cgit[<0.12] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 14 Jan 2015 ]
    token = security
    description = [ CVE-2016-1899, CVE-2016-1900, CVE-2016-1901 ]
]]

net-misc/socat[<1.7.3.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Feb 2015 ]
    token = security
    description = [ http://www.dest-unreach.org/socat/contrib/socat-secadv7.html
                    http://www.dest-unreach.org/socat/contrib/socat-secadv8.html ]
]]

(
    dev-db/mariadb[<10.2.38]
    dev-db/mariadb[>=10.3&<10.3.29]
    dev-db/mariadb[>=10.4&<10.4.19]
    dev-db/mariadb[>=10.5&<10.5.10]
) [[
    *author = [ Heiko Becker <heirecka@exherbo.org> ]
    *date = [ 26 May 2021 ]
    *token = security
    *description = [ CVE-2021-2154, CVE-2021-2166 ]
]]

net-fs/samba[<4.14.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 May 2021 ]
    token = security
    description = [ CVE-2021-20254 ]
]]

net-mail/dovecot[<2.3.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Jun 2021 ]
    token = security
    description = [ CVE-2021-29157, CVE-2021-33515 ]
]]

www-servers/apache[<2.4.46] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 05 Aug ]
    token = security
    description = [ CVE-2020-{9490,11984,11985,11993} ]
]]

dev-scm/libgit2[<0.27.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 06 Oct 2018 ]
    token = security
    description = [ CVE-2018-17456 ]
]]

net-irc/weechat[<1.9.1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 30 Sep 2017 ]
    token = security
    description = [ CVE-2017-14727 ]
]]

media-video/rtmpdump[<2.4_p20151223] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2015-827{0,1,2} ]
]]

net-mail/tnef[<1.4.18] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Oct 2020 ]
    token = security
    description = [ CVE-2019-18849 ]
]]

net-wireless/hostapd[<2.9] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Oct 2020 ]
    token = security
    description = [ CVE-2019-949{4,5,6,7,8}, CVE-2019-16275, CVE-2020-12695 ]
]]

net-remote/teamviewer[<13.2.13582] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Aug 2018 ]
    token = security
    description = [ CVE-2018-143333 ]
]]

sys-auth/sssd[<1.16.1-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Aug 2018 ]
    token = security
    description = [ CVE-2018-10852 ]
]]

net/mosquitto[<2.0.10] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Apr 2021 ]
    token = security
    description = [ CVE-2021-28166 ]
]]

net-libs/nghttp2[<1.41.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2020 ]
    token = security
    description = [ CVE-2020-11080 ]
]]

net-irc/znc[<1.8.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 Jun 2020 ]
    token = security
    description = [ CVE-2020-13775 ]
]]

net-p2p/transmission[<2.94] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Jul 2018 ]
    token = security
    description = [ CVE-2018-5702 ]
]]

net-analyzer/ettercap[<0.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Jan 2019 ]
    token = security
    description = [ CVE-2014-{6395,6396,9376,9377,9378,9379,9380,9381}, CVE-2017-6430 ]
]]

net/synapse[<1.33.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 May 2021 ]
    token = security
    description = [ CVE-2021-29471 ]
]]

net-libs/zeromq[<4.3.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Sep 2020 ]
    token = security
    description = [ CVE-2020-15166 ]
]]

net/gitea[<1.7.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Apr 2019 ]
    token = security
    description = [ CVE-2019-11228, CVE-2019-11229 ]
]]

net/gogs[<0.11.91] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Nov 2019 ]
    token = security
    description = [ CVE-2019-14544 ]
]]

www-servers/lighttpd[<1.4.54] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Nov 2019 ]
    token = security
    description = [ CVE-2019-11072 ]
]]

sys-cluster/ceph[<14.2.20] [[
    author = [ Arnaud Lefebvre <a.lefebvre@outlook.fr> ]
    date = [ 15 May 2021 ]
    token = security
    description = [ CVE-2021-3509, CVE-2021-3524, CVE-2021-3531 ]
]]

dev-db/redis[<6.2.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Jul 2021 ]
    token = security
    description = [ CVE-2021-32625 ]
]]

www-servers/tomcat-bin[<8.5.63] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Mar 2021 ]
    token = security
    description = [ CVE-2021-25122, CVE-2021-25329 ]
]]

net/coturn[<4.5.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Jan 2021 ]
    token = security
    description = [ CVE-2020-26262 ]
]]

web-apps/mailman[<2.1.33] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 11 May 2020 ]
    token = security
    description = [ CVE-2020-12137 ]
]]

net/solr[<8.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Jun 2021 ]
    token = security
    description = [ CVE-2021-27905, CVE-2021-29262, CVE-2021-29943 ]
]]

net-libs/libmaxminddb[<1.4.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Nov 2020 ]
    token = security
    description = [ CVE-2020-28241 ]
]]

net-mail/dovecot-pigeonhole[<0.5.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Jun 2021 ]
    token = security
    description = [ CVE-2020-28200 ]
]]
